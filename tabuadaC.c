#include <stdio.h>
#include <stdlib.h>

int main () {
	int n1;
	int multiplicador = 0;
	int resultado;

	printf("Por favor digite o numero da tabuada requerida: ");
	scanf("%d", &n1);

	while (multiplicador <= 10) {
		resultado = n1 * multiplicador;
		printf("A tabuada de %d x %d eh igual a %d\n",n1,multiplicador,resultado);
		multiplicador++;
	}
}
