#include <stdio.h>
#include <stdlib.h>

int main () {
	int n1;
	int n2;
	int operacao;
	int resultado;

	printf("Por favor digite o primeiro numero: ");
	scanf("%d", &n1);

	printf("Por favor digite o segundo numero: ");
	scanf("%d", &n2);

	printf("Por favor digite o numero da operacao desejada, de acordo com esta descricao: 1 - SOMA,  2 - SUBTRACAO, 3 - MULTIPLICACAO, 4 - DIVISAO ");
	scanf("%d", &operacao);

	if (operacao == 1) {
		resultado = n1 + n2;
		printf("O resultado de sua operacao eh: %d\n", resultado);
	}
	if (operacao == 2) {
		resultado = n1 - n2;
		printf("O resultado de sua operacao eh: %d\n", resultado);
	}
	if (operacao == 3) {
		resultado = n1 * n2;
		printf("O resultado de sua operacao eh: %d\n", resultado);
	}
	if (operacao == 4) {
		resultado = n1 / n2;
		printf("O resultado de sua operacao eh: %d\n", resultado);
	}
}
